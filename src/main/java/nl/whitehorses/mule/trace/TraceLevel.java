package nl.whitehorses.mule.trace;

import org.apache.log4j.Level;

/**
 * Enumeration containing available log levels.
 */
public enum TraceLevel {

	/**
	 * Log level for trace data.
	 */
	TRACE(Level.TRACE),

	/**
	 * Log level for debug data.
	 */
	DEBUG(Level.DEBUG),

	/**
	 * Log level for info data, this is the default {@code MuleSoft} log level.
	 */
	INFO(Level.INFO),

	/**
	 * Log level for warnings.
	 */
	WARNING(Level.WARN),

	/**
	 * Log level for errors.
	 */
	ERROR(Level.ERROR);

	private final Level log4jLevel;

	/**
	 * Default constructor.
	 * 
	 * @param log4jLevel
	 *            Log4j {@link Level}.
	 */
	private TraceLevel(final Level log4jLevel) {
		this.log4jLevel = log4jLevel;
	}

	/**
	 * Get log4j {@link Level}.
	 * 
	 * @return Log4j {@link Level}.
	 */
	protected Level getLog4jLevel() {
		return this.log4jLevel;
	}

}