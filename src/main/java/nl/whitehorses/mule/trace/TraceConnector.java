package nl.whitehorses.mule.trace;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.annotations.Connector;
import org.mule.api.annotations.Processor;
import org.mule.api.annotations.param.Default;
import org.mule.api.annotations.param.Optional;
import org.mule.api.construct.FlowConstruct;
import org.mule.util.StringUtils;

@Connector(name = "trace", friendlyName = "Trace")
public class TraceConnector {

	// Logging
	private final static Logger LOG = Logger.getLogger(TraceConnector.class.getName());

	// Inbound- and outbound property
	private static final String PROPERTY_TRACE_ID = "trace_id_header";

	// Flowvar
	private static final String FLOWVAR_TRACE_ID = "traceId";

	/**
	 * Default constructor.
	 */
	public TraceConnector() {
		super();
	}

	/**
	 * Processor to initialize {@code flowVar} named {@code traceId} for trace data.
	 * <p>
	 * This processor should be used immediately after the inbound transport
	 * barriers.
	 * 
	 * @param event
	 *            {@link MuleEvent} that will be injected automatically by DevKit.
	 * @param defaultTraceId
	 *            Optional trace ID. If this parameter is filled, the provided trace
	 *            ID will be used. Otherwise the component will try to obtain the
	 *            trace ID from the inbound properties. If this is empty as well,
	 *            the message unique ID will be used.
	 */
	@Processor(friendlyName = "Initialize trace ID")
	public void initTraceId(final MuleEvent event, @Optional final String defaultTraceId) {
		final MuleMessage message = event.getMessage();
		String traceId = defaultTraceId;
		if (StringUtils.isBlank(traceId)) {
			final String traceIdInboundProperty = message.getInboundProperty(PROPERTY_TRACE_ID);
			traceId = StringUtils.isNotBlank(traceIdInboundProperty) ? traceIdInboundProperty : message.getUniqueId();
		}
		event.setFlowVariable(FLOWVAR_TRACE_ID, traceId);
	}

	/**
	 * Processor to initialize outbound property {@code trace_id_header} from the
	 * {@code traceId flowVar}.
	 * <p>
	 * The {@link #initTraceId(MuleEvent, String)} processor hould be used before
	 * this processor in the flow. Otherwise the {@code traceId flowVar} might not
	 * be initialized.
	 * 
	 * @param event
	 *            {@link MuleEvent} that will be injected automatically by DevKit.
	 */
	@Processor(friendlyName = "Initialize trace outbound properties")
	public void initTraceOutboundProperties(final MuleEvent event) {
		final String traceId = (String) event.getFlowVariable(FLOWVAR_TRACE_ID);
		if (StringUtils.isNotBlank(traceId)) {
			event.getMessage().setOutboundProperty(PROPERTY_TRACE_ID, traceId);
		}
	}

	/**
	 * Processor to log message with trace data.
	 * <p>
	 * The {@link #initTraceId(MuleEvent, String)} processor should be used before
	 * this processor in the flow. Otherwise the {@code traceId flowVar} might not be
	 * initialized.
	 * 
	 * @param event
	 *            {@link MuleEvent} that will be injected automatically by DevKit.
	 * @param level
	 *            Log level, the default value will be {@code INFO}.
	 * @param phase
	 *            Phase of the flow, the default value will be {@code OTHER}.
	 * @param message
	 *            Log message.
	 * @param metaData
	 *            Optional {@link Map} where meta data can be added to the log message.
	 */
	@Processor(friendlyName = "Log message with trace data")
	public void logTrace(final MuleEvent event, @Default("INFO") final TraceLevel level, @Default("OTHER") final TracePhase phase, final String message, @Optional final Map<String, String> metaData) {
		final String traceId = (String) event.getFlowVariable(FLOWVAR_TRACE_ID);
		final StringBuilder logBuilder = new StringBuilder("{");
		logBuilder.append("\n\t\"messageId\": \"").append(event.getMessage().getUniqueId()).append("\",");
		logBuilder.append("\n\t\"traceId\": \"").append(traceId).append("\",");
		final FlowConstruct flowConstruct = event.getFlowConstruct();
		if(flowConstruct != null) {
			logBuilder.append("\n\t\"flow\": \"").append(flowConstruct.getName()).append("\",");
		}
		logBuilder.append("\n\t\"phase\": \"").append(phase.name()).append("\",");
		if (metaData != null && !metaData.isEmpty()) {
			logBuilder.append("\n\t\"metaData\": {");
			final Set<String> metaDataKeySet = metaData.keySet();
			final Iterator<String> metaDataKeyIterator = metaDataKeySet.iterator();
			for(int i = 0; i < metaDataKeySet.size(); i++) {
				final String metaDataKey = metaDataKeyIterator.next();
				final String metaDataValue = metaData.get(metaDataKey);
				logBuilder.append("\n\t\t\"").append(metaDataKey).append("\": \"").append(metaDataValue).append("\"");
				if(i < (metaDataKeySet.size() - 1)) {
					logBuilder.append(",");
				}
			}
			logBuilder.append("\n\t},");
		}
		logBuilder.append("\n\t\"message\": \"").append(message).append("\"");
		logBuilder.append("\n}");
		LOG.log(level.getLog4jLevel(), logBuilder.toString());
	}
}