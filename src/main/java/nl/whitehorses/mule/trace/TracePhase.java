package nl.whitehorses.mule.trace;

/**
 * Enumeration containing all log phases.
 */
public enum TracePhase {
	
	/**
	 * Start of the Mule flow.
	 */
	START,
	
	/**
	 * End of the Mule flow.
	 */
	END,
	
	/**
	 * Before (outbound) Transport Barrier.
	 */
	BEFORE_TRANSPORT,
	
	/**
	 * After (outbound) Transport Barrier.
	 */
	AFTER_TRANSPORT,
	
	/**
	 * Exception handler of a flow.
	 */
	EXCEPTION,
	
	/**
	 * Other phase in the flow.
	 */
	OTHER
}
