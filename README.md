# Mule Trace Connector (Anypoint Connector)
The mule-trace-connector is a custom Mule connector, which can be used for message tracing or end-to-end logging.

# Mule supported versions
Mule 3.9.0, 3.9.1

# Installation
You can download the source code and build it with devkit to find it available on your local repository. Then you can add it to Studio.

# Usage
For information about usage our documentation at https://bitbucket.org/whitehorsesbv/mule-trace-connector.